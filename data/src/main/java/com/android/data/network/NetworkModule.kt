package com.android.data.network

import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Module
class NetworkModule {

    @Provides
    @Reusable
    fun postDataService(dataServiceFactory: DataServiceFactory): DataServicePost =
        dataServiceFactory.create(DataServicePost::class.java)

    @Provides
    @Reusable
    fun commentDataService(dataServiceFactory: DataServiceFactory): DataServiceComment =
        dataServiceFactory.create(DataServiceComment::class.java)

}