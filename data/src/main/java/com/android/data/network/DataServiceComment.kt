package com.android.data.network

import com.android.data.entity.model.remote.Comment
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by hassanalizadeh on 31,December,2019
 */
interface DataServiceComment {
    @GET("/posts/{postId}/comments")
    fun comments(@Path("postId") postId: Int): Single<List<Comment>>
}