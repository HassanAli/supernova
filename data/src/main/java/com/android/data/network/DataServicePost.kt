package com.android.data.network

import com.android.data.entity.model.remote.Post
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by hassanalizadeh on 26,December,2019
 */
interface DataServicePost {
    @GET("/users/1/posts")
    fun posts(): Single<List<Post>>
}