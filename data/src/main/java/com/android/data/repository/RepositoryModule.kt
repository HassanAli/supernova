package com.android.data.repository

import com.android.data.repository.datasource.DataSourceModule
import com.android.domain.repository.CommentRepository
import com.android.domain.repository.PostRepository
import dagger.Binds
import dagger.Module
import dagger.Reusable

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module(includes = [DataSourceModule::class])
abstract class RepositoryModule {

    @Binds
    @Reusable
    abstract fun repositoryPost(repository: PostRepositoryImpl): PostRepository

    @Binds
    @Reusable
    abstract fun repositoryComment(repository: CommentRepositoryImpl): CommentRepository

}