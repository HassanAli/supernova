package com.android.data.repository.datasource

import com.android.data.repository.datasource.comment.DataSourceComment
import com.android.data.repository.datasource.comment.SmartCommentDataSource
import com.android.data.repository.datasource.post.DataSourcePost
import com.android.data.repository.datasource.post.SmartPostDataSource
import dagger.Binds
import dagger.Module
import dagger.Reusable

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module
abstract class DataSourceModule {

    @Binds
    @Reusable
    abstract fun dataSourcePost(smartPostDataSource: SmartPostDataSource): DataSourcePost

    @Binds
    @Reusable
    abstract fun dataSourceComment(smartCommentDataSource: SmartCommentDataSource): DataSourceComment

}