package com.android.data.repository

import com.android.data.entity.mapper.toCommentObject
import com.android.data.repository.datasource.comment.DataSourceComment
import com.android.domain.entity.comment.CommentObject
import com.android.domain.repository.CommentRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 01,January,2020
 */
class CommentRepositoryImpl @Inject constructor(
    private val smart: DataSourceComment
) : CommentRepository {
    override fun comments(postId: Int): Single<List<CommentObject>> = smart.comments(postId)
        .map { comments ->
            comments.filter { it.postId == postId }.let {
                it.map { it.toCommentObject() }
            }
        }

}