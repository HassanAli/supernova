package com.android.data.repository.datasource.post

import com.android.data.entity.dao.PostDao
import com.android.data.entity.model.local.PostEntity
import com.android.data.entity.model.remote.Post
import com.android.data.network.DataServicePost
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class SmartPostDataSource @Inject constructor(
    private val service: DataServicePost,
    private val postDao: PostDao
) : DataSourcePost {

    override fun posts(): Single<List<Post>> = service.posts()

    override fun insert(posts: List<PostEntity>): Completable {
        postDao.deleteAll()
        postDao.insert(posts)
        return Completable.complete()
    }

    override fun loadPosts(): Flowable<List<PostEntity>> = postDao.selectAll()

    override fun post(postId: Int): Single<PostEntity> = postDao.select(postId)

}