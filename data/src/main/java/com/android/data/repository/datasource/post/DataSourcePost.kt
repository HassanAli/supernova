package com.android.data.repository.datasource.post

import com.android.data.entity.model.local.PostEntity
import com.android.data.entity.model.remote.Post
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 27,December,2019
 */
interface DataSourcePost {

    /**
     * Get post by id
     * */
    fun post(postId: Int): Single<PostEntity>

    /**
     * Get posts from rest api
     * */
    fun posts(): Single<List<Post>>

    /**
     * Clear post table
     * Insert into table
     * */
    fun insert(posts: List<PostEntity>): Completable

    /**
     * Get posts from DB
     * */
    fun loadPosts(): Flowable<List<PostEntity>>

}