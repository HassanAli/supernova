package com.android.data.repository

import com.android.data.entity.mapper.mapToEntity
import com.android.data.entity.mapper.toPostObject
import com.android.data.repository.datasource.post.DataSourcePost
import com.android.domain.entity.post.PostObject
import com.android.domain.repository.PostRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class PostRepositoryImpl @Inject constructor(
    private val smart: DataSourcePost
) : PostRepository {

    override fun posts(): Completable = smart.posts().map { posts ->
        posts.filter { it.userId == 1 }.map { it.mapToEntity() }.let {
            smart.insert(it)
        }
    }.ignoreElement()

    override fun loadPosts(): Flowable<List<PostObject>> = smart.loadPosts().map { posts ->
        posts.map { it.toPostObject() }
    }

    override fun loadPost(postId: Int): Single<PostObject> =
        smart.post(postId).map { it.toPostObject() }

}