package com.android.data.repository.datasource.comment

import com.android.data.entity.model.remote.Comment
import com.android.data.network.DataServiceComment
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 01,January,2020
 */
class SmartCommentDataSource @Inject constructor(
    private val service: DataServiceComment
) : DataSourceComment {

    override fun comments(postId: Int): Single<List<Comment>> = service.comments(postId)

}