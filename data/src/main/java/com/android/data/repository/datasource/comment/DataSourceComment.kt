package com.android.data.repository.datasource.comment

import com.android.data.entity.model.remote.Comment
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 31,December,2019
 */
interface DataSourceComment {
    fun comments(postId: Int): Single<List<Comment>>
}