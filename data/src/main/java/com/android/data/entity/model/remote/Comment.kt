package com.android.data.entity.model.remote

/**
 * Created by hassanalizadeh on 31,December,2019
 */
data class Comment(
    val postId: Int,
    val id: Int,
    val name: String,
    val email: String,
    val body: String
)