package com.android.data.entity.model.remote

/**
 * Created by hassanalizadeh on 26,December,2019
 */
data class Post(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String
)