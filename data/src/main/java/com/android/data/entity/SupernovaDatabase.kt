package com.android.data.entity

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.data.entity.dao.PostDao
import com.android.data.entity.model.local.PostEntity

/**
 * Created by hassanalizadeh on 28,December,2019
 */
@Database(
    entities = [PostEntity::class],
    version = 1,
    exportSchema = false
)
abstract class SupernovaDatabase : RoomDatabase() {

    abstract fun postDao(): PostDao

}