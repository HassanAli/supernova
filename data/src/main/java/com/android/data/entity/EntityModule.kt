package com.android.data.entity

import android.app.Application
import androidx.room.Room
import com.android.data.entity.dao.PostDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by hassanalizadeh on 28,December,2019
 */
@Module
class EntityModule {

    @Provides
    fun postDao(db: SupernovaDatabase): PostDao = db.postDao()

    @Provides
    @Singleton
    fun database(application: Application): SupernovaDatabase = Room.databaseBuilder(
        application.applicationContext,
        SupernovaDatabase::class.java,
        "supernova_db"
    ).build()

}