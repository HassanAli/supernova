package com.android.data.entity.mapper

import com.android.data.entity.model.remote.Comment
import com.android.data.entity.model.remote.Post
import com.android.domain.entity.comment.CommentObject
import com.android.domain.entity.post.PostObject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
fun Post.toPostObject(): PostObject =
    PostObject(
        id = id,
        title = title,
        body = body
    )

fun Comment.toCommentObject(): CommentObject =
    CommentObject(
        postId = postId, id = id, name = name, email = email, body = body
    )