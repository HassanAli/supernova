package com.android.data.entity.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.data.entity.model.local.PostEntity
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 28,December,2019
 */
@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<PostEntity>)

    @Query("SELECT * FROM post WHERE id=:postId LIMIT 1")
    fun select(postId: Int): Single<PostEntity>

    @Query("SELECT * FROM post ORDER BY id")
    fun selectAll(): Flowable<List<PostEntity>>

    @Query("DELETE FROM post")
    fun deleteAll()

}