package com.android.data.entity.mapper

import com.android.data.entity.model.local.PostEntity
import com.android.data.entity.model.remote.Post

/**
 * Created by hassanalizadeh on 28,December,2019
 */
fun Post.mapToEntity(): PostEntity = PostEntity(
    id, userId, title, body
)