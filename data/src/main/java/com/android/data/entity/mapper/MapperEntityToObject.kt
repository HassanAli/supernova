package com.android.data.entity.mapper

import com.android.data.entity.model.local.PostEntity
import com.android.domain.entity.post.PostObject

/**
 * Created by hassanalizadeh on 28,December,2019
 */
fun PostEntity.toPostObject(): PostObject =
    PostObject(id, title, body)