package com.android.data.entity.model.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by hassanalizadeh on 28,December,2019
 */
@Entity(tableName = "post")
data class PostEntity(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "user_id")
    val userId: Int,
    val title: String,
    val body: String
)