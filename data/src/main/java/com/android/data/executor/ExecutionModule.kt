package com.android.data.executor

import com.android.domain.executor.ThreadExecutor
import dagger.Binds
import dagger.Module

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Module
abstract class ExecutionModule {

    @Binds
    abstract fun threadExecutor(executor: JobExecutor): ThreadExecutor

}