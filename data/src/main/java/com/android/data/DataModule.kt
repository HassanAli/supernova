package com.android.data

import com.android.data.entity.EntityModule
import com.android.data.executor.ExecutionModule
import com.android.data.network.NetworkModule
import com.android.data.repository.RepositoryModule
import dagger.Module

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Module(
    includes = [
        ExecutionModule::class,
        NetworkModule::class,
        EntityModule::class,
        RepositoryModule::class
    ]
)
abstract class DataModule