package com.android.presentation

import com.android.presentation.common.CommonModule
import dagger.Module

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Module(
    includes = [
        CommonModule::class
    ]
)
abstract class PresentationModule