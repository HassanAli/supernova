package com.android.presentation.ui.detail

import androidx.appcompat.app.AppCompatActivity
import com.android.presentation.common.BaseActivityModule
import com.android.presentation.common.di.PerActivity
import com.android.presentation.common.di.PerFragment
import com.android.presentation.ui.detail.fragment.view.DetailFragment
import com.android.presentation.ui.detail.fragment.view.DetailFragmentModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by hassanalizadeh on 03,January,2020
 */
@Module(includes = [BaseActivityModule::class])
abstract class DetailActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = [DetailFragmentModule::class])
    abstract fun fragmentInjector(): DetailFragment

    @Binds
    @PerActivity
    abstract fun activity(activity: DetailActivity): AppCompatActivity

}