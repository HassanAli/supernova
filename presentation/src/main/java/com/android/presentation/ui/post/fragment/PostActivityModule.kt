package com.android.presentation.ui.post.fragment

import androidx.appcompat.app.AppCompatActivity
import com.android.presentation.common.BaseActivityModule
import com.android.presentation.common.di.PerActivity
import com.android.presentation.common.di.PerFragment
import com.android.presentation.ui.post.PostNavigator
import com.android.presentation.ui.post.fragment.view.PostFragment
import com.android.presentation.ui.post.fragment.view.PostFragmentModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module(includes = [BaseActivityModule::class])
abstract class PostActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = [PostFragmentModule::class])
    abstract fun fragmentInjector(): PostFragment

    @Binds
    @PerActivity
    abstract fun activity(activity: PostActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun postNavigator(activity: PostActivity): PostNavigator

}