package com.android.presentation.ui.detail.fragment.viewmodel

import androidx.lifecycle.MutableLiveData
import com.android.domain.entity.comment.DetailObject
import com.android.domain.usecase.detail.GetDetailUseCase
import com.android.presentation.common.view.BaseViewModel
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 03,January,2020
 */
class DetailViewModel @Inject constructor(
    private val getDetailUseCase: GetDetailUseCase
) : BaseViewModel() {

    val detail: MutableLiveData<DetailObject> = MutableLiveData()

    fun detail(postId: Int) {
        getDetailUseCase.invoke(postId)
            .subscribe({
                detail.value = it
            }, {})
            .track()
    }

}