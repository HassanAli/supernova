package com.android.presentation.ui.post.fragment

import android.os.Bundle
import com.android.presentation.R
import com.android.presentation.common.BaseActivity
import com.android.presentation.ui.post.PostNavigator
import com.android.presentation.ui.post.fragment.view.PostFragment

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class PostActivity : BaseActivity(), PostNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null)
            addFragment(R.id.fragmentContainer, PostFragment.newInstance())
    }

    override fun showDetail(postId: Int) {
        navigator.toDetail(this, postId)
    }

}