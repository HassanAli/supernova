package com.android.presentation.ui.detail.fragment.adapter

import android.view.View
import com.android.domain.entity.comment.CommentObject
import com.android.presentation.common.adapter.BaseViewHolder
import com.android.presentation.common.adapter.ViewTypeHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_comment.*

/**
 * Created by hassanalizadeh on 03,January,2020
 */
class CommentViewHolder constructor(override val containerView: View) :
    BaseViewHolder<CommentObject>(containerView), LayoutContainer {

    override fun getType(): Int = ViewTypeHolder.COMMENT_VIEW

    override fun bind(data: CommentObject?) {
        data ?: return
        adapterCommentName.text = data.name
        adapterCommentEmail.text = data.email
        adapterCommentText.text = data.body
    }

}