package com.android.presentation.ui.post.fragment.adapter

import android.view.View
import com.android.domain.entity.post.PostObject
import com.android.presentation.common.adapter.BaseViewHolder
import com.android.presentation.common.adapter.ViewTypeHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_post.*

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class PostViewHolder(override val containerView: View) : BaseViewHolder<PostObject>(containerView),
    LayoutContainer {

    override fun getType(): Int = ViewTypeHolder.POST_VIEW

    override fun bind(data: PostObject?) {
        data ?: return
        adapterAlbumTitle.text = data.title
        adapterAlbumBody.text = data.body
        adapterPostRoot.setOnClickListener {
            mSubject.onNext(SelectPostAction(data.id))
        }
    }

}