package com.android.presentation.ui.detail

import android.os.Bundle
import com.android.presentation.R
import com.android.presentation.common.BaseActivity
import com.android.presentation.ui.detail.fragment.view.DetailFragment

/**
 * Created by hassanalizadeh on 03,January,2020
 */
class DetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val postId = intent?.extras?.getInt(POST_ID) ?: 0
            addFragment(R.id.fragmentContainer, DetailFragment.newInstance(postId))
        }
    }

    companion object {
        const val POST_ID = "Post_Id"
    }

}