package com.android.presentation.ui.post

/**
 * Created by hassanalizadeh on 31,December,2019
 */
interface PostNavigator {
    fun showDetail(postId: Int)
}