package com.android.presentation.ui.post.fragment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import com.android.domain.entity.post.PostObject
import com.android.domain.usecase.invoke
import com.android.domain.usecase.post.GetPostsUseCase
import com.android.domain.usecase.post.RefreshPostsUseCase
import com.android.presentation.common.adapter.BaseAction
import com.android.presentation.common.view.BaseViewModel
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class PostViewModel @Inject constructor(
    private val getPostsUseCase: GetPostsUseCase,
    private val refreshPostsUseCase: RefreshPostsUseCase
) : BaseViewModel() {

    val posts: LiveData<List<PostObject>> =
        LiveDataReactiveStreams.fromPublisher(getPostsUseCase.invoke())
    val isLoading = MutableLiveData<Boolean>(false)
    val clickObserver = MutableLiveData<BaseAction>()

    init {
        refreshPosts()
    }

    fun refreshPosts() {
        isLoading.value = true
        refreshPostsUseCase.invoke()
            .subscribe({
                isLoading.value = false
            }, {
                isLoading.value = false
            })
            .track()
    }

    fun observeClicks(observable: Observable<BaseAction>) {
        observable.subscribe({
            clickObserver.value = it
        }, {

        }).track()
    }

}