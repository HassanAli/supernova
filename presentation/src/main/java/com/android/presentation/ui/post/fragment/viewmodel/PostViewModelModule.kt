package com.android.presentation.ui.post.fragment.viewmodel

import androidx.lifecycle.ViewModel
import com.android.presentation.common.di.PerFragment
import com.android.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module
abstract class PostViewModelModule {

    @Binds
    @PerFragment
    @IntoMap
    @ViewModelKey(PostViewModel::class)
    abstract fun viewModel(viewModel: PostViewModel): ViewModel

}