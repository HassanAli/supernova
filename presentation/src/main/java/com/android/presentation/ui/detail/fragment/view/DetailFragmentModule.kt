package com.android.presentation.ui.detail.fragment.view

import androidx.fragment.app.Fragment
import com.android.presentation.common.di.PerFragment
import com.android.presentation.common.view.BaseFragmentModule
import com.android.presentation.ui.detail.fragment.viewmodel.DetailViewModelModule
import dagger.Binds
import dagger.Module
import javax.inject.Named

/**
 * Created by hassanalizadeh on 03,January,2020
 */
@Module(includes = [BaseFragmentModule::class, DetailViewModelModule::class])
abstract class DetailFragmentModule {

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract fun fragment(detailFragment: DetailFragment): Fragment

}