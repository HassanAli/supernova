package com.android.presentation.ui.post.fragment.view

import androidx.fragment.app.Fragment
import com.android.presentation.common.di.PerFragment
import com.android.presentation.common.view.BaseFragmentModule
import com.android.presentation.ui.post.fragment.viewmodel.PostViewModelModule
import dagger.Binds
import dagger.Module
import javax.inject.Named

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module(includes = [BaseFragmentModule::class, PostViewModelModule::class])
abstract class PostFragmentModule {

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract fun fragment(fragment: PostFragment): Fragment

}
