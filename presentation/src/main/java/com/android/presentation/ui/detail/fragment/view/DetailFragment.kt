package com.android.presentation.ui.detail.fragment.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.domain.entity.comment.DetailObject
import com.android.presentation.R
import com.android.presentation.common.extension.linearLayout
import com.android.presentation.common.extension.observe
import com.android.presentation.common.extension.viewModelProvider
import com.android.presentation.common.view.BaseFragment
import com.android.presentation.common.viewmodel.ViewModelProviderFactory
import com.android.presentation.ui.detail.DetailActivity.Companion.POST_ID
import com.android.presentation.ui.detail.fragment.adapter.CommentAdapter
import com.android.presentation.ui.detail.fragment.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_main.recyclerView
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 03,January,2020
 */
class DetailFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: DetailViewModel
    private lateinit var adapter: CommentAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)
        adapter = CommentAdapter()


        observe(viewModel.detail, ::observeDetail)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_detail, container, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        recyclerView.linearLayout(
            context = activityContext,
            spacing = resources.getDimensionPixelSize(R.dimen.size_small_2)
        )
        recyclerView.adapter = adapter

        if (savedInstanceState == null) {
            val postId = arguments?.getInt(POST_ID) ?: 0
            viewModel.detail(postId)
        }
    }

    private fun observeDetail(detail: DetailObject) {
        detailFragmentTitle.text = detail.postObject.title
        detailFragmentBody.text = detail.postObject.body
        adapter.addItems(detail.comments)
    }

    companion object {
        fun newInstance(postId: Int): DetailFragment {
            val fragment = DetailFragment()
            val bundle = Bundle().apply {
                putInt(POST_ID, postId)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

}