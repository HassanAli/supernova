package com.android.presentation.ui.detail.fragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.domain.entity.comment.CommentObject
import com.android.presentation.common.adapter.BaseRecyclerAdapter
import com.android.presentation.common.adapter.BaseViewHolder
import com.android.presentation.common.adapter.EmptyViewHolder
import com.android.presentation.common.adapter.ViewTypeHolder

/**
 * Created by hassanalizadeh on 03,January,2020
 */
class CommentAdapter : BaseRecyclerAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return when (viewType) {
            ViewTypeHolder.COMMENT_VIEW -> CommentViewHolder(view)
            else -> EmptyViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        super.onBindViewHolder(holder, position)

        when (holder.getType()) {
            ViewTypeHolder.COMMENT_VIEW -> (holder as CommentViewHolder).bind(mItems[position] as? CommentObject)
            else -> (holder as EmptyViewHolder).bind(Unit)
        }
    }

}