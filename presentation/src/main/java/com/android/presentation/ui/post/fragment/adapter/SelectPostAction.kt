package com.android.presentation.ui.post.fragment.adapter

import com.android.presentation.common.adapter.ActionType
import com.android.presentation.common.adapter.BaseAction

/**
 * Created by hassanalizadeh on 31,December,2019
 */
class SelectPostAction(val data: Int) : BaseAction {
    override fun getType(): ActionType = ActionType.SELECT_POST
}