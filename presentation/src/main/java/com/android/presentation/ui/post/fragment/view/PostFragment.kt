package com.android.presentation.ui.post.fragment.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.presentation.R
import com.android.presentation.common.adapter.BaseAction
import com.android.presentation.common.extension.linearLayout
import com.android.presentation.common.extension.observe
import com.android.presentation.common.extension.viewModelProvider
import com.android.presentation.common.view.BaseFragment
import com.android.presentation.common.viewmodel.ViewModelProviderFactory
import com.android.presentation.ui.post.PostNavigator
import com.android.presentation.ui.post.fragment.adapter.PostAdapter
import com.android.presentation.ui.post.fragment.adapter.SelectPostAction
import com.android.presentation.ui.post.fragment.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class PostFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var factory: ViewModelProviderFactory
    @Inject
    lateinit var navigator: PostNavigator

    private lateinit var viewModel: PostViewModel
    private lateinit var adapter: PostAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)
        adapter = PostAdapter { holder ->
            // observe clicks
            viewModel.observeClicks(holder.observe())
        }

        observe(viewModel.posts) { adapter.addItems(it) }
        observe(viewModel.isLoading, ::observeRefreshing)
        observe(viewModel.clickObserver, ::observeClicks)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        loadingIndicator.setOnRefreshListener(this)
        recyclerView.linearLayout(
            context = activityContext,
            spacing = resources.getDimensionPixelSize(R.dimen.size_small_2)
        )
        recyclerView.adapter = adapter
    }

    override fun onRefresh() {
        viewModel.refreshPosts()
    }

    private fun observeRefreshing(status: Boolean) {
        if (status) {
            showLoading(loadingIndicator)
        } else {
            hideLoading(loadingIndicator)
        }
    }

    private fun observeClicks(action: BaseAction) {
        when (action) {
            is SelectPostAction -> {
                navigator.showDetail(action.data)
            }
        }
    }

    companion object {
        fun newInstance(): PostFragment = PostFragment()
    }

}