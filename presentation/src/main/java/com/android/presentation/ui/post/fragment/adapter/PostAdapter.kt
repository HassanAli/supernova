package com.android.presentation.ui.post.fragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.domain.entity.post.PostObject
import com.android.presentation.common.adapter.*

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class PostAdapter(
    private val listener: (holder: BaseViewHolder<*>) -> Unit
) : BaseRecyclerAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        val holder: BaseViewHolder<out Any> = when (viewType) {
            ViewTypeHolder.POST_VIEW -> PostViewHolder(view)
            ViewTypeHolder.LOAD_MORE_VIEW -> LoadMoreViewHolder(view)
            else -> EmptyViewHolder(view)
        }

        listener.invoke(holder)
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        super.onBindViewHolder(holder, position)

        when (holder.getType()) {
            ViewTypeHolder.POST_VIEW -> (holder as PostViewHolder).bind(mItems[position] as? PostObject)
            ViewTypeHolder.LOAD_MORE_VIEW -> (holder as LoadMoreViewHolder).bind(Unit)
            else -> (holder as EmptyViewHolder).bind(Unit)
        }
    }

}