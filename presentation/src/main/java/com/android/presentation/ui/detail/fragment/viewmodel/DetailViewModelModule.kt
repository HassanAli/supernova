package com.android.presentation.ui.detail.fragment.viewmodel

import androidx.lifecycle.ViewModel
import com.android.presentation.common.di.PerFragment
import com.android.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by hassanalizadeh on 03,January,2020
 */
@Module
abstract class DetailViewModelModule {

    @Binds
    @PerFragment
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun viewModel(detailViewModel: DetailViewModel): ViewModel

}