package com.android.presentation.common.extension

import android.graphics.Bitmap
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

/**
 * Created by hassanalizadeh on 24,May,2019
 */
internal fun AppCompatImageView.load(url: String?, roundedCorners: Int = 8) {
    Glide.with(context)
        .load(url)
        .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(roundedCorners)))
        .into(this)
}

internal fun AppCompatImageView.load(
    url: String?,
    @DrawableRes placeholder: Int,
    transformation: Transformation<Bitmap>?
) {
    Glide.with(context)
        .load(url)
        .placeholder(placeholder)
        .apply { if (transformation != null) apply(RequestOptions().transform(transformation)) }
        .into(this)
}
