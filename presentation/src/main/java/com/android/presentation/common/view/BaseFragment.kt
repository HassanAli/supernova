package com.android.presentation.common.view

import android.content.Context
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 24,May,2019
 */
abstract class BaseFragment : Fragment(), HasSupportFragmentInjector {

    @Inject
    protected lateinit var activityContext: Context

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>


    override fun onAttach(context: Context) {
        // This is called even for API levels below 23 if we didn't use AppCompat.
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun supportFragmentInjector() = childFragmentInjector

    protected fun addChildFragment(@IdRes containerViewId: Int, fragment: Fragment) =
        childFragmentManager.beginTransaction()
            .add(containerViewId, fragment)
            .commit()

    fun showMessage(@StringRes resourceId: Int) {
        showMessage(getString(resourceId))
    }

    fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    protected fun hideLoading(loadingIndicator: SwipeRefreshLayout) {
        if (loadingIndicator.isRefreshing)
            loadingIndicator.post { loadingIndicator.isRefreshing = false }
    }

    protected fun showLoading(loadingIndicator: SwipeRefreshLayout) {
        if (!loadingIndicator.isRefreshing)
            loadingIndicator.post { loadingIndicator.isRefreshing = true }
    }

}