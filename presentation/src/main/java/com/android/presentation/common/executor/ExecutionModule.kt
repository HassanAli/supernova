package com.android.presentation.common.executor

import com.android.domain.executor.PostExecutionThread
import dagger.Binds
import dagger.Module

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module
abstract class ExecutionModule {

    @Binds
    abstract fun postExecutionThread(thread: UIThread): PostExecutionThread

}