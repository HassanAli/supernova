package com.android.presentation.common.extension

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations

/**
 * Created by hassanalizadeh on 24,May,2019
 */
internal fun <X, Y> LiveData<X>.map(body: (X) -> Y): LiveData<Y> = Transformations.map(this, body)

internal fun <X, Y> LiveData<X>.switchMap(body: (X) -> LiveData<Y>): LiveData<Y> = Transformations.switchMap(this, body)