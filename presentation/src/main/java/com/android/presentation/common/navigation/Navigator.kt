package com.android.presentation.common.navigation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.presentation.ui.detail.DetailActivity
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class Navigator @Inject constructor() {

    fun toDetail(activity: AppCompatActivity, postId: Int) {
        val intent = Intent(activity, DetailActivity::class.java)
        val bundle = Bundle().apply {
            putInt(
                DetailActivity.POST_ID,
                postId
            )
        }
        intent.putExtras(bundle)
        activity.startActivity(intent)
    }

}