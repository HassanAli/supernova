package com.android.presentation.common.adapter

import com.android.domain.entity.DomainObject

/**
 * Created by hassanalizadeh on 26,November,2019
 */
interface IRecyclerAdapter {

    fun <T : DomainObject> addItem(index: Int, item: T)

    fun <T : DomainObject> addItem(item: T)

    fun <T : DomainObject> addItems(items: List<T>?)

    fun remove(index: Int)

    fun removeAll()

}