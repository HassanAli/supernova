package com.android.presentation.common

import com.android.presentation.common.executor.ExecutionModule
import dagger.Module

/**
 * Created by hassanalizadeh on 27,December,2019
 */
@Module(includes = [ExecutionModule::class])
abstract class CommonModule