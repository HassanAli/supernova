package com.android.presentation.common.view

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.android.presentation.common.di.PerFragment
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Created by hassanalizadeh on 24,May,2019
 */
@Module
class BaseFragmentModule {

    @Provides
    @Named
    @PerFragment
    fun childFragmentManager(@Named(FRAGMENT) fragment: Fragment): FragmentManager = fragment.childFragmentManager

    companion object {
        const val FRAGMENT = "BaseFragmentModule.fragment"
        const val CHILD_FRAGMENT_MANAGER = "BaseFragmentModule.childFragmentManager"
    }

}