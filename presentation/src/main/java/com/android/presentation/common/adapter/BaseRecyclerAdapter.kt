package com.android.presentation.common.adapter

import android.os.Handler
import androidx.recyclerview.widget.RecyclerView
import com.android.domain.entity.DomainObject
import com.android.domain.entity.LoadMoreObject
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

/**
 * Created by hassanalizadeh on 26,November,2019
 */
abstract class BaseRecyclerAdapter() : RecyclerView.Adapter<BaseViewHolder<*>>(), IRecyclerAdapter {

    private val loadMoreObservable: PublishSubject<Boolean> = PublishSubject.create()
    private var isLoading: Boolean = false
    protected var mItems: MutableList<DomainObject> = mutableListOf()
    private var mConfig: Config? = null
    private val disposable = CompositeDisposable()

    constructor(config: Config) : this() {
        mConfig = config
        subscriberLoadMore()
    }

    override fun getItemViewType(position: Int): Int {
        return ViewTypeHolder.getView(mItems[position]::class)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val config = mConfig ?: return
        if (isLoading) return
        // if (mItems.size < config.screenSize) return
        if (mItems.size < config.preFetchCount) return
        if (position < (mItems.size - config.preFetchCount)) return

        loadMoreObservable.onNext(true)
    }

    override fun <T : DomainObject> addItem(item: T) {
        this.mItems.add(item)
        notifyItemInserted(mItems.size - 1)
    }

    override fun <T : DomainObject> addItem(index: Int, item: T) {
        this.mItems.add(index, item)
        notifyItemInserted(index)
    }

    override fun <T : DomainObject> addItems(items: List<T>?) {
        items ?: return
        removeAll()
        this.mItems.addAll(items)
        notifyDataSetChanged()
    }

    override fun remove(index: Int) {
        if (mItems.size > 0) return
        mItems.removeAt(index)
        notifyItemRemoved(index)
    }

    override fun removeAll() {
        mItems.clear()
        notifyDataSetChanged()
    }

    fun getLoadMoreObservable(): PublishSubject<Boolean> = loadMoreObservable

    private fun subscriberLoadMore() {
        disposable.add(
            loadMoreObservable
                .subscribe({
                    isLoading = it
                    if (isLoading && mItems[mItems.size - 1] !is LoadMoreObject)
                        addLoadMoreToList()
                    else if (!isLoading && mItems[mItems.size - 1] is LoadMoreObject)
                        removeLoadMoreFromList()
                }, {
                    removeLoadMoreFromList()
                })
        )
    }

    private fun addLoadMoreToList() {
        if (!isLoading) return
        Handler().post {
            this.mItems.add(LoadMoreObject())
            notifyItemInserted(mItems.size - 1)
        }
    }

    private fun removeLoadMoreFromList() {
        Handler().post {
            val position = mItems.size - 1
            mItems.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        disposable.clear()
    }

    companion object {
        const val FIRST = 0
    }
}

class Config private constructor(
    val preFetchCount: Int,
    val screenSize: Int
) {
    class Builder {
        private var preFetchCount = 0
        private var screenSize = 0
        fun setPreFetch(size: Int) = apply { preFetchCount = size }
        fun setScreenSize(size: Int) = apply { screenSize = size }
        fun build() = Config(preFetchCount, screenSize)
    }
}