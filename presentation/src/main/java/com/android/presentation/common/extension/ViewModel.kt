package com.android.presentation.common.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.android.presentation.common.viewmodel.ViewModelProviderFactory

/**
 * Created by hassanalizadeh on 24,May,2019
 */
internal fun <T> LifecycleOwner.observe(liveData: LiveData<T>?, action: (t: T) -> Unit) {
    liveData?.observe(this, Observer { t -> action(t) })
}

internal inline fun <reified VM : ViewModel> Fragment.viewModelProvider(provider: ViewModelProviderFactory) =
    ViewModelProviders.of(this, provider)[VM::class.java]