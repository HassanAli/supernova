package com.android.presentation.common.adapter

import com.android.domain.entity.DomainObject
import com.android.domain.entity.LoadMoreObject
import com.android.domain.entity.comment.CommentObject
import com.android.domain.entity.post.PostObject
import com.android.presentation.R
import kotlin.reflect.KClass

/**
 * Created by hassanalizadeh on 26,November,2019
 */
object ViewTypeHolder {

    val POST_VIEW = R.layout.adapter_post
    val COMMENT_VIEW = R.layout.adapter_comment
    val EMPTY_VIEW = R.layout.adapter_empty
    val LOAD_MORE_VIEW = R.layout.adapter_load_more

    fun getView(clazz: KClass<out DomainObject>): Int {
        return when (clazz) {
            PostObject::class -> POST_VIEW
            CommentObject::class -> COMMENT_VIEW
            LoadMoreObject::class -> LOAD_MORE_VIEW
            else -> EMPTY_VIEW
        }
    }

}