package com.android.presentation.common.view

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.android.presentation.common.di.PerChildFragment
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Created by hassanalizadeh on 24,May,2019
 */
@Module
class BaseChildFragmentModule {

    @Provides
    @Named(Companion.SECOND_CHILD_FRAGMENT_MANAGER)
    @PerChildFragment
    fun childFragmentManager(@Named(CHILD_FRAGMENT) fragment: Fragment): FragmentManager =
        fragment.childFragmentManager


    companion object {
        const val CHILD_FRAGMENT = "BaseChildFragmentModule.childFragment"
        const val SECOND_CHILD_FRAGMENT_MANAGER = "BaseChildFragmentModule.childFragmentManager"
    }

}