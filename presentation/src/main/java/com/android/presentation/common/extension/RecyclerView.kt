package com.android.presentation.common.extension

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.presentation.common.utils.GridSpacingDecoration

/**
 * Created by hassanalizadeh on 24,May,2019
 */
internal fun RecyclerView.linearLayout(
    context: Context,
    spacing: Int,
    spanCount: Int? = 1,
    @RecyclerView.Orientation orientation: Int? = RecyclerView.VERTICAL,
    reverseLayout: Boolean? = false
) {
    layoutManager = LinearLayoutManager(context, orientation!!, reverseLayout!!)
    setHasFixedSize(true)
    addItemDecoration(
        GridSpacingDecoration(
            spanCount = spanCount!!,
            spacing = spacing,
            includeEdge = true,
            headerNum = 0
        )
    )
}