package com.android.presentation.common.di

import javax.inject.Scope

/**
 * Created by hassanalizadeh on 24,May,2019
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment