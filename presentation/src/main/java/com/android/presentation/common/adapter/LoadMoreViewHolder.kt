package com.android.presentation.common.adapter

import android.view.View
import kotlinx.android.extensions.LayoutContainer

/**
 * Created by hassanalizadeh on 26,November,2019
 */
class LoadMoreViewHolder(override val containerView: View) :
    BaseViewHolder<Unit>(containerView),
    LayoutContainer {

    override fun getType(): Int = ViewTypeHolder.LOAD_MORE_VIEW

    override fun bind(data: Unit?) {

    }

}