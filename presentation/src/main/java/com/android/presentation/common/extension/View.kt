package com.android.presentation.common.extension

import android.view.View

/**
 * Created by hassanalizadeh on 24,May,2019
 */

internal fun View.gone() {
    visibility = View.GONE
}

internal fun View.visible() {
    visibility = View.VISIBLE
}

internal fun View.invisible() {
    visibility = View.INVISIBLE
}
