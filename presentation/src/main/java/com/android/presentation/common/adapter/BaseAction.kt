package com.android.presentation.common.adapter

/**
 * Created by hassanalizadeh on 26,November,2019
 */
interface BaseAction {
    fun getType(): ActionType
}