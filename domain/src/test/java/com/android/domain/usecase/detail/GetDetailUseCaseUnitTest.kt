package com.android.domain.usecase.detail

import com.android.common_test.TestUtil
import com.android.common_test.mock
import com.android.common_test.transformer.TestSTransformer
import com.android.domain.entity.comment.DetailObject
import com.android.domain.repository.CommentRepository
import com.android.domain.repository.PostRepository
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

/**
 * Created by hassanalizadeh on 21,June,2020
 */
@RunWith(JUnit4::class)
class GetDetailUseCaseUnitTest {

    private val postRepository: PostRepository = mock()
    private val commentRepository: CommentRepository = mock()

    private var useCase: GetDetailUseCase =
        GetDetailUseCase(postRepository, commentRepository, TestSTransformer())


    @Test
    fun `get post and its comments`() {
        // GIVEN
        `when`(postRepository.loadPost(anyInt())).thenReturn(Single.just(TestUtil.fakePostObject()))
        `when`(commentRepository.comments(anyInt())).thenReturn(Single.just(TestUtil.fakeCommentListObject()))

        // WHEN
        useCase.invoke(TestUtil.fakePostObject().id)
            .test()
            .assertValue(DetailObject(TestUtil.fakePostObject(), TestUtil.fakeCommentListObject()))
            .assertComplete()

        // THEN
        verify(postRepository).loadPost(TestUtil.fakePostObject().id)
        verify(commentRepository).comments(TestUtil.fakePostObject().id)
    }

}