package com.android.domain.usecase.post

import com.android.common_test.TestUtil
import com.android.common_test.mock
import com.android.common_test.transformer.TestFTransformer
import com.android.domain.repository.PostRepository
import com.android.domain.usecase.invoke
import io.reactivex.Flowable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

/**
 * Created by hassanalizadeh on 21,June,2020
 */
@RunWith(JUnit4::class)
class GetPostsUseCaseUnitTest {

    private val postRepository: PostRepository = mock()
    private val useCase: GetPostsUseCase = GetPostsUseCase(postRepository, TestFTransformer())


    @Test
    fun `get posts`() {
        // GIVEN
        `when`(postRepository.loadPosts()).thenReturn(Flowable.just(TestUtil.fakePostListObject()))

        // WHEN
        useCase.invoke()
            .test()
            .assertValue(TestUtil.fakePostListObject())
            .assertComplete()

        // THEN
        verify(postRepository).loadPosts()
    }

}