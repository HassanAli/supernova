package com.android.domain.usecase.post

import com.android.common_test.mock
import com.android.common_test.transformer.TestCTransformer
import com.android.domain.repository.PostRepository
import com.android.domain.usecase.invoke
import io.reactivex.Completable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

/**
 * Created by hassanalizadeh on 21,June,2020
 */
@RunWith(JUnit4::class)
class RefreshPostsUseCaseUnitTest {

    private val postRepository: PostRepository = mock()
    private val useCase: RefreshPostsUseCase =
        RefreshPostsUseCase(postRepository, TestCTransformer())


    @Test
    fun `get posts`() {
        // GIVEN
        `when`(postRepository.posts()).thenReturn(Completable.complete())

        // WHEN
        useCase.invoke()
            .test()
            .assertComplete()

        // THEN
        verify(postRepository).posts()
    }

}