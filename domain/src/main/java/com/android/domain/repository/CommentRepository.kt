package com.android.domain.repository

import com.android.domain.entity.comment.CommentObject
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 01,January,2020
 */
interface CommentRepository {
    fun comments(postId: Int): Single<List<CommentObject>>
}