package com.android.domain.repository

import com.android.domain.entity.post.PostObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 27,December,2019
 */
interface PostRepository {

    /**
     * Refresh posts from rest api
     * */
    fun posts(): Completable

    /**
     * Get posts fomr DB
     * */
    fun loadPosts(): Flowable<List<PostObject>>

    /**
     * Get post by id
     * */
    fun loadPost(postId: Int): Single<PostObject>

}