package com.android.domain.executor.transformer

import io.reactivex.CompletableTransformer

/**
 * Transformer to io thread for completable.
 */
abstract class CTransformer : CompletableTransformer