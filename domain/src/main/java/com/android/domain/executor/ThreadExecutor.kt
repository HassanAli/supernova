package com.android.domain.executor

import java.util.concurrent.Executor

/**
 * Created by hassanalizadeh on 26,December,2019
 */
interface ThreadExecutor : Executor