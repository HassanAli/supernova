package com.android.domain

import com.android.domain.entity.comment.DetailObject
import com.android.domain.entity.post.PostObject
import com.android.domain.executor.transformer.*
import dagger.Binds
import dagger.Module
import dagger.Reusable

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Module
abstract class DomainModule {

    @Binds
    @Reusable
    abstract fun completableTransformer(
        transformer: AsyncCTransformer
    ): CTransformer

    @Binds
    @Reusable
    abstract fun postTransformer(
        transformer: AsyncFTransformer<List<PostObject>>
    ): FTransformer<List<PostObject>>

    @Binds
    @Reusable
    abstract fun detailTransformer(
        transformer: AsyncSTransformer<DetailObject>
    ): STransformer<DetailObject>

}