package com.android.domain.entity.comment

import com.android.domain.entity.post.PostObject

/**
 * Created by hassanalizadeh on 01,January,2020
 */
data class DetailObject(
    val postObject: PostObject,
    val comments: List<CommentObject>?
)