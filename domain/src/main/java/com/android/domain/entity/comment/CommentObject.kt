package com.android.domain.entity.comment

import com.android.domain.entity.DomainObject

/**
 * Created by hassanalizadeh on 01,January,2020
 */
data class CommentObject(
    val postId: Int,
    val id: Int,
    val name: String,
    val email: String,
    val body: String
): DomainObject