package com.android.domain.entity.post

import com.android.domain.entity.DomainObject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
data class PostObject(
    val id: Int,
    val title: String,
    val body: String
) : DomainObject