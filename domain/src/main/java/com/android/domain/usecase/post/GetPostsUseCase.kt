package com.android.domain.usecase.post

import com.android.domain.entity.post.PostObject
import com.android.domain.executor.transformer.FTransformer
import com.android.domain.repository.PostRepository
import com.android.domain.usecase.UseCaseFlowable
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 27,December,2019
 */
class GetPostsUseCase @Inject constructor(
    private val postRepository: PostRepository,
    private val transformer: FTransformer<List<PostObject>>
) : UseCaseFlowable<List<PostObject>, Unit>() {

    override fun execute(param: Unit): Flowable<List<PostObject>> =
        postRepository.loadPosts().compose(transformer)

}