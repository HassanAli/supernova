package com.android.domain.usecase.detail

import com.android.domain.entity.comment.DetailObject
import com.android.domain.executor.transformer.STransformer
import com.android.domain.repository.CommentRepository
import com.android.domain.repository.PostRepository
import com.android.domain.usecase.UseCaseSingle
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 01,January,2020
 */
class GetDetailUseCase @Inject constructor(
    private val postRepository: PostRepository,
    private val commentRepository: CommentRepository,
    private val transformer: STransformer<DetailObject>
) : UseCaseSingle<DetailObject, Int>() {

    override fun execute(param: Int): Single<DetailObject> =
        flatMap(
            { postRepository.loadPost(param) },
            { commentRepository.comments(param) },
            { post, comments -> DetailObject(post, comments) }
        ).compose(transformer)

}

fun <A, B, R> flatMap(
    zero: () -> Single<A>,
    one: () -> Single<B>,
    result: (A, B) -> R
): Single<R> = zero.invoke()
    .flatMap { A ->
        one.invoke()
            .map { B ->
                result.invoke(A, B)
            }
    }