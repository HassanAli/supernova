package com.android.domain.usecase.post

import com.android.domain.executor.transformer.CTransformer
import com.android.domain.repository.PostRepository
import com.android.domain.usecase.UseCaseCompletable
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 28,December,2019
 */
class RefreshPostsUseCase @Inject constructor(
    private val postRepository: PostRepository,
    private val transformer: CTransformer
) : UseCaseCompletable<Unit>() {

    override fun execute(param: Unit): Completable = postRepository.posts().compose(transformer)

}