package com.android.common_test

import com.android.domain.entity.comment.CommentObject
import com.android.domain.entity.post.PostObject

/**
 * Created by hassanalizadeh on 21,June,2020
 */
object TestUtil {
    fun fakePostObject(): PostObject = PostObject(100, "Fake Title", "Fake Body")
    fun fakePostListObject(): List<PostObject> = listOf(fakePostObject())
    fun fakeCommentObject(): CommentObject =
        CommentObject(100, 1, "FakeName", "Fake Email", "Fake Body")

    fun fakeCommentListObject(): List<CommentObject> = listOf(fakeCommentObject())
}