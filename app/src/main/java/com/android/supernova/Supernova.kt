package com.android.supernova

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * Created by hassanalizadeh on 26,December,2019
 */
class Supernova : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerSupernovaComponent.builder().create(this)

}