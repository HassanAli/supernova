package com.android.supernova

import com.android.data.DataModule
import com.android.domain.DomainModule
import com.android.presentation.PresentationModule
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Singleton
@Component(
    modules = [
        SupernovaModule::class,
        DataModule::class,
        DomainModule::class,
        PresentationModule::class
    ]
)
interface SupernovaComponent : AndroidInjector<Supernova> {

    /**
     * Builder for this component
     * */
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<Supernova>()

}