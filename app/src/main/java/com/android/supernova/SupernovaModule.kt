package com.android.supernova

import android.app.Application
import com.android.presentation.common.di.PerActivity
import com.android.presentation.ui.detail.DetailActivity
import com.android.presentation.ui.detail.DetailActivityModule
import com.android.presentation.ui.post.fragment.PostActivity
import com.android.presentation.ui.post.fragment.PostActivityModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by hassanalizadeh on 26,December,2019
 */
@Module(includes = [AndroidSupportInjectionModule::class])
abstract class SupernovaModule {

    @Binds
    @Singleton
    abstract fun application(application: Supernova): Application

    @PerActivity
    @ContributesAndroidInjector(modules = [PostActivityModule::class])
    abstract fun postActivityInjector(): PostActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DetailActivityModule::class])
    abstract fun detailActivityInjector(): DetailActivity

}